# Showmitch &mdash; Show Image

Setting the monitor background to a random image with XFCE4's desktop
settings misses some features I would like to have

* It picks images at random in a weird way or is even broken, as I
  tend to see groups of images repeatedly as if the random number
  generator somehow "clusters". As it is supposed to be random, it is
  hard to complain, since even showing the same image 1000 times is
  still correct for a truely random infinite sequence. Yet to overcome
  this problem, Showmitch maintains a list of images it has shown
  already and only when the list is emptied, it is reload with all
  images.
* It is cumbersome to figure out which image is currently
  shown. Showmitch paints some image information onto a temporary copy
  of the image to show as the background.
* In addition, Showmitch allows to permanently blacklist individual
  images. This is a text file with just path's of images not to show.

# How to use

The source file `./src/main/java/Showmitch.java` is standalone and can
be run by any modern `java` program as a script. The following is an
example which works for me as a cron job:

```
SHELL=/bin/bash
DISPLAY=:0.0

17,43,59 * * * * DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$(id -u)/bus" java path/to/Showmitch.java -s $HOME/data/Showmitch/shown.txt -b $HOME/data/Showmitch/blacklisted.txt -t $HOME/data/Showmitch/tracking.txt $HOME/Photos/????-??-* >>$HOME/data/Showmitch/cron.out 2>&1
```

**NOTE** the peculiar `DBUS_SESSION_BUS_ADDRESS` setting. This may or
may not work on your system. Check with `echo
$DBUS_SESSION_BUS_ADDRESS` if this is right for you. I am
investigating how to simplify this.

The command line options, all required, are:
* `-t trackingfile` specifies where to log which images was placed on
  which monitor and when 
* `-b blacklist` a text file which could contain individual image
  paths to never show
* `-s shown` a text file used to record the images shown from the
  current set of images. It will be automatically cleared and
  restarted once all images have been shown.
* All other parameters should be directory paths which are searched
  for image files.

  
## Feature Ideas
* Add dbus detection such that this can be easily run from a `cron` job.
* Better guessing or configuration of which meta data of the image to
  paint on it before setting it as a background.
* Enable one-click blacklisting by providing a script that picks the
  current image path and puts it into the blacklist
* Separate image "management" from image conversion and painting and
  from finally showing it to make the latter two steps more
  configurable or pluggable.


