import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * <p>NOTE: for xfce4 see <a href="https://unix.stackexchange.com/a/596112/140503">stackexchange</a></p>
 */
public class Showmitch {
  private static final Logger LOG = Logger.getLogger(Showmitch.class.getName());
  private static final Set<String> IMAGEEXTENTIONS = Set.of("jpg", "jpeg", "png");
  private static final SecureRandom rand = new SecureRandom();

  private record Args(Set<Path> imageDirs, Path shownImages, Path blacklisted,
                      Path tracking)
  {
    private Args {
      Objects.requireNonNull(imageDirs, "imageDirs");
      Objects.requireNonNull(shownImages, "shownImages");
      Objects.requireNonNull(blacklisted, "blacklisted");
      Objects.requireNonNull(tracking, "tracking");
    }
  }

  public static void main(String[] argv) {
    try {
      innerMain(argv);
    } catch (Throwable e) {
      LOG.log(Level.SEVERE, "unspecific failure, see stack trace", e);
    }
  }

  private static void innerMain(String[] argv)
      throws IOException, InterruptedException {
    setupLogging();
    Args args = parseArgs(argv);
    Set<Path> images = getImagePaths(args);
    LOG.info("Found " + images.size() + " image files, next: ");
    for (String monitorAndScreen : monitorOnScreen()) {
      if (images.size() == 0) {
        Files.deleteIfExists(args.shownImages());
        images = getImagePaths(args);
      }
      Path nextImage = nextImage(args, images);
      setBackground(monitorAndScreen, nextImage);
      Files.writeString(
          args.tracking,
          Instant.now() + " " + monitorAndScreen + ' ' + nextImage + '\n',
          StandardCharsets.UTF_8,
          StandardOpenOption.APPEND,
          StandardOpenOption.CREATE
      );
    }
  }

  private static Path nextImage(Args args, Set<Path> available) throws IOException {
    if (available.size() < 1) {
      throw new IllegalStateException("don't have any image");
    }
    List<Path> all = new ArrayList<>(available);
    int picked = rand.nextInt(all.size());
    Path result = all.get(picked);
    available.remove(result);
    Files.writeString(
        args.shownImages(),
        result.toString() + '\n',
        StandardCharsets.UTF_8,
        StandardOpenOption.APPEND,
        StandardOpenOption.CREATE
    );
    return result;
  }

  private static Set<Path> getImagePaths(Args args) throws IOException {
    Set<Path> images = new HashSet<>(1000);
    for (Path p : args.imageDirs()) {
      findImageFiles(images, p);
    }
    Set<Path> shownImages = readFileList(args.shownImages());
    Set<Path> blacklisted = readFileList(args.blacklisted());
    images.removeAll(shownImages);
    images.removeAll(blacklisted);
    return images;
  }

  private static Set<Path> readFileList(Path filePath) throws IOException {
    if (!Files.isReadable(filePath)) {
      return Set.of();
    }
    Set<Path> result = new HashSet<>();
    for (var fname : Files.readAllLines(filePath, StandardCharsets.UTF_8)) {
      result.add(Paths.get(fname));
    }
    return result;
  }

  private static void findImageFiles(Set<Path> result, Path dir) throws IOException {
    try (var paths = Files.newDirectoryStream(dir)) {
      for (var p : paths) {
        if (Files.isDirectory(p)) {
          findImageFiles(result, p);
          continue;
        }
        if (isImage(p)) {
          result.add(p);
        }
      }
    }
  }

  private static Args parseArgs(String[] argv) {
    Set<Path> imageDirs = new HashSet<>();
    Path shownImages = null;
    Path blacklisted = null;
    Path tracking = null;
    for (int i = 0; i < argv.length; /**/) {
      String arg = argv[i++];
      if (arg.equals("-s")) {
        shownImages = i < argv.length ? Paths.get(argv[i]) : null;
        i += 1;
        continue;
      }
      if (arg.equals("-b")) {
        blacklisted = i < argv.length ? Paths.get(argv[i]) : null;
        i += 1;
        continue;
      }
      if (arg.equals("-t")) {
        tracking = i < argv.length ? Paths.get(argv[i]) : null;
        i += 1;
        continue;
      }
      imageDirs.add(Paths.get(arg));
    }
    return new Args(imageDirs, shownImages, blacklisted, tracking);
  }

  private static boolean isImage(Path p) {
    String pName = p.toString();
    int dotIndex = pName.lastIndexOf('.');
    if (dotIndex < 0) {
      return false;
    }
    String extension = pName.substring(dotIndex + 1).toLowerCase(Locale.ROOT);
    return IMAGEEXTENTIONS.contains(extension);
  }

  private static void setBackground(String screenMonitor, Path imagePath)
      throws IOException, InterruptedException {
    // FIXME: Rather ask xrandr for "*/backdrop-cycle-random-order" and compare
    // with screenMonitor to figure out which workspace is relevant.
    String property = "/backdrop/" + screenMonitor + "/workspace0/last-image";
    Path annotatedImage = convert(imagePath);
    Process xfconf = new ProcessBuilder(
        "xfconf-query",
        "-c",
        "xfce4-desktop",
        "-p",
        property,
        "-s",
        annotatedImage.toString()
    )
        .redirectError(ProcessBuilder.Redirect.INHERIT)
        .redirectOutput(ProcessBuilder.Redirect.INHERIT)
        .start();
    String commandLine = xfconf.info().commandLine().orElseGet(() -> "");
    int code = xfconf.waitFor();
    if (code > 0) {
      LOG.severe("failed with exit code " + code + ": " + commandLine);
    } else {
      LOG.info(commandLine);
    }
    LOG.info("background set to " + imagePath);
  }

  /**
   * Copies the image to a temporary file while annotating it. The temporary file
   * will always be in the JPEG format.
   * <p>NOTE: I accidently used PNG at the start, but then {@code convert} is
   * really slow.</p>
   * @param imagePath to convert
   * @return the name of the temporary file to use
   */
  private static Path convert(Path imagePath)
      throws IOException, InterruptedException {
    int length = imagePath.getNameCount();
    String text;
    if (length >= 2) {
      text = imagePath.getName(length - 2).toString();
    } else {
      text = imagePath.toString();
    }

    Path result = Files.createTempFile("Showmitch", ".jpg");
    Process convert = new ProcessBuilder(
        "convert",
        "-auto-orient",
        "-fill",
        "white",
        "-pointsize",
        "60",
        "-weight",
        "900",
        "-gravity",
        "SouthEast",
        "-draw",
        "text 100,100 '" + text + "'",
        imagePath.toString(),
        result.toString()
    )
        .redirectError(ProcessBuilder.Redirect.INHERIT)
        .redirectOutput(ProcessBuilder.Redirect.INHERIT)
        .start();
    //System.out.println("started " + convert.info().commandLine());
    convert.waitFor();
    return result;
  }

  /**
   * Determines strings like {@code screen<id>/monitor<id>} from xrandr output.
   * Though I have no chance yet to test for anything but screen 0.
   *
   * @return A list of screen+monitor paths to be used as part of properties for
   * xconf-query
   * @throws IOException if calling xrandr fails
   */
  private static List<String> monitorOnScreen() throws IOException {
    Process xrandr = new ProcessBuilder("xrandr")
        .redirectError(ProcessBuilder.Redirect.INHERIT)
        .start();
    List<String> result = new ArrayList<>(3);
    try (BufferedReader rin = new BufferedReader(new InputStreamReader(xrandr.getInputStream()))) {
      String line;
      String screen = null;
      while (null != (line = rin.readLine())) {
        String[] parts = line.split(" ");
        if (parts.length < 2) {
          continue;
        }
        if ("Screen".equals(parts[0])) {
          screen = parts[1].substring(0, parts[1].length() - 1);
          continue;
        }
        if ("connected".equals(parts[1])) {
          result.add("screen" + screen + "/monitor" + parts[0]);
        }
      }
    }
    return result;
  }

  private static void setupLogging() {
    Logger rootLogger = Logger.getLogger("");
    for (var h : rootLogger.getHandlers()) {
      rootLogger.removeHandler(h);
    }
    ConsoleHandler ch = new ConsoleHandler();
    ch.setLevel(Level.WARNING);
    DateTimeFormatter dateTimeFormat = DateTimeFormatter
        .ofPattern("uuuu-MM-dd'T'HH:mm:ss'Z'")
        .withZone(ZoneId.of("UTC"));
    ch.setFormatter(new Formatter() {
      @Override
      public String format(LogRecord record) {
        return dateTimeFormat.format(record.getInstant()) + " "
               + record.getMessage() + ' ' + record.getLevel() + '\n';
      }
    });
    rootLogger.addHandler(ch);
  }
}
